import { Action, NotFoundError } from 'routing-controllers';

import { TokenService } from './../services';
import { User } from './../models';
import { DataProvider } from './../utils';
import { decode } from 'punycode';

export const AuthorizationChecker = async (action: Action) => {
    try {
        const token = action.request.headers.authorization;
        if (!token) return false;
        const decoded = TokenService.decode(token);
        if (!decoded) return false;
        return true;
    } catch (e) {
        return false;
    }
}