import { Service } from 'typedi';
import { Repository } from 'typeorm';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { User } from './../models';

@Service()
export class AuthService {

    @OrmRepository(User)
    private readonly repository: Repository<User>;

    public async login(email: string, password: string) {
        const user = await this.repository.findOne({ email, password });
        return user;
    }
}