import {
  JsonController,
  Get,
  Post,
  Put,
  Delete,
  Param,
  CurrentUser,
  Body,
  HttpCode,
  Authorized,
} from 'routing-controllers';

import { User } from './../models';
import { UserService, AuthService, TokenService } from './../services';
import { UserRequest } from './../requests';

@JsonController('/user')
export class UserController {

  constructor(private readonly userService: UserService,
    private readonly authService: AuthService) { }

  @Authorized()
  @Get('/')
  public async fetchAll() {
    return await this.userService.findAll();
  }

  @Get('/:id')
  public async fetchOne( @Param('id') id: number) {
    return this.userService.findOne(id);
  }

  @Post('/')
  public async create( @Body({ required: true }) userRequest: UserRequest) {
    const { email, password, first_name, last_name } = userRequest;
    const exist = await this.userService.exist(email);
    if (exist) {
      throw new Error('User alreade exist');
    }

    const encodePass = TokenService.encodePass(password);

    const newUser = await this.userService.create(new User(<User>{ email, password: encodePass, first_name, last_name }));

    const token = TokenService.encode({ email });

    return { token };
  }

  @Put('/:id')
  public async update(
    @CurrentUser({ required: true }) user: User,
    @Body({ required: true }) userRequest: UserRequest
    ) {
    user.update(<User>userRequest);
    return this.userService.update(user);
  }

  @Delete('/:id')
  @HttpCode(204)
  public async remove( @CurrentUser({ required: true }) user: User) {
    await this.userService.remove(user);
    return true;
  }
}
