import {
    JsonController,
    Get,
    Post,
    Put,
    Delete,
    Param,
    CurrentUser,
    Body,
    HttpCode,
    Authorized,
} from 'routing-controllers';

import { LoginRequest } from './../requests';
import { AuthService, TokenService } from './../services';

@JsonController('/auth')
export class AuthController {

    constructor(private readonly authService: AuthService) { }

    @Post('/login')
    public async login( @Body({ required: true }) loginRequest: LoginRequest) {
        const { email, password } = loginRequest;

        const encodePass = TokenService.encodePass(password);

        const user = await this.authService.login(email, encodePass);
        if (!user) throw new Error('Incorrect email or password');

        const token = TokenService.encode({ email });
        return { token };
    }
}