import { IsString, IsNotEmpty, IsEmail } from 'class-validator';

export class LoginRequest {

  @IsEmail()
  @IsNotEmpty()
  public email: string;

  @IsString()
  @IsNotEmpty()
  public password: string;
}